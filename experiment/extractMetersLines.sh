#!/usr/bin/bash
DATASETFOLDER=/home/magirrezaba008/CorpusSonetosSigloDeOro/SEQ2SEQDATA

cd ~/CorpusSonetosSigloDeOro/rhythm/CorpusSonetosSigloDeOro-output
cat */*.xml | grep "<ns0:l met" | sed 's/.*met=\"//;s/\".*\">/\t/;s/<\/.*//' > ${DATASETFOLDER}/metLine.txt


#This part was got+adapted from http://stackoverflow.com/questions/7764755/unix-how-to-split-a-file-into-equal-parts-without-breaking-individual-lines
# Configuration stuff
fspec=${DATASETFOLDER}/metLine.txt
num_files=20

# Work out lines per file.

total_lines=$(wc -l <${fspec})
((lines_per_file = (total_lines + num_files - 1) / num_files))

# Split the actual file, maintaining lines.

split --lines=${lines_per_file} -d ${fspec} ${DATASETFOLDER}/xyzzy.

# Debug information
#echo "Total lines     = ${total_lines}"
#echo "Lines  per file = ${lines_per_file}"    
#wc -l ${DATASETFOLDER}/xyzzy.*


for (( i=0; i<19; i=i+2 )) 
do
  istr=$(printf %02d $i)
  iincstr=$(printf %02d $((i+1)))
  TESTFILE=${DATASETFOLDER}/xyzzy.${istr}
  DEVFILE=${DATASETFOLDER}/xyzzy.${iincstr}
  TRAINFILEOUTPUT=${DATASETFOLDER}/train-${istr}.txt
  TESTFILEOUTPUT=${DATASETFOLDER}/test-${istr}.txt
  DEVFILEOUTPUT=${DATASETFOLDER}/dev-${istr}.txt

  echo "Removing file " ${TRAINFILEOUTPUT}
  echo "Removing file " ${TESTFILEOUTPUT}
  echo "Removing file " ${DEVFILEOUTPUT}
  rm ${TRAINFILEOUTPUT}
  rm ${TESTFILEOUTPUT}
  rm ${DEVFILEOUTPUT}

  cp $TESTFILE ${TESTFILEOUTPUT}
  echo "Copying file " $TESTFILE " into " ${TESTFILEOUTPUT}
  cp $DEVFILE ${DEVFILEOUTPUT}
  echo "Copying file " $DEVFILE " into " ${DEVFILEOUTPUT}

  for (( j=0; j<$i; j=j+1 )) 
  do
    jstr=$(printf %02d $j)
    echo "Dumping file" ${DATASETFOLDER}/xyzzy.${jstr} " into file " ${TRAINFILEOUTPUT}
    cat ${DATASETFOLDER}/xyzzy.${jstr} >> ${TRAINFILEOUTPUT}
  done
  for (( j=$i+2; j<=19; j=j+1 )) 
  do
    jstr=$(printf %02d $j)
    echo "Dumping file" ${DATASETFOLDER}/xyzzy.${jstr} " into file " ${TRAINFILEOUTPUT}
    cat ${DATASETFOLDER}/xyzzy.${jstr} >> ${TRAINFILEOUTPUT}
  done
  echo
done

rm ${DATASETFOLDER}/xyzzy.*


cat metLine.txt | cut -f1 > meters.txt
cat metLine.txt | cut -f2 > lines.txt



