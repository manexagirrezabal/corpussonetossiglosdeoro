
# -*- coding: utf-8

import sys
import glob
import os
import xml.etree.ElementTree as ET
import re
import string
import foma
from spanishstress import setstress
from ambiguity import getAmbiguousFirst

HOMEDIR="/home/magirrezaba008/"
FST=foma.FST()
FSM=FST.load(HOMEDIR+"/Dropbox/thesis/syllableGoldStandard/ruleSyllabifiers/silabaEs.fst")

def analyzeWord (w):
  
  res=FSM.apply_down(w)
  if res is None:
    return '+?'
  else:
    return res

def buscarVocalVocal(wordlist):
  #This line returns a list of positions in which a synaloepha can be made (without including syllables starting with 'h')
#  return [((sylllist[i+1][0] in vowels) and (sylllist[i][-1] in vowels)) for i in xrange(len(sylllist)-1) if (sylllist[i]!="" and sylllist[i+1]!="")]
  #This line returns a list of possible synaloephas between words, and synaloephas between syllables of the same word are not allowed
  return [(len(word)-1==inds) and syll[-1] in vowels and safe_list_get(wordlist,indw+1,'_')[0][0] in vowels for indw,word in enumerate(wordlist) for inds,syll in enumerate(word)]
  #Origin [(syll,(len(word)-1==inds),syll[-1] in vowels,safe_list_get(wordlist,indw+1,'_')[0][0] in vowels) for indw,word in enumerate(wordlist) for inds,syll in enumerate(word)]

def buscarVocalVocalh(wordlist):
  #This line returns a list of positions in which a synaloepha can be made (including syllables starting with 'h')
#  return [((sylllist[i+1][0] in vowelsh) and (sylllist[i][-1] in vowels)) for i in xrange(len(sylllist)-1) if (sylllist[i]!="" and sylllist[i+1]!="")]
  #This line returns a list
  return [(len(word)-1==inds) and syll[-1] in vowelsh and safe_list_get(wordlist,indw+1,'_')[0][0] in vowelsh for indw,word in enumerate(wordlist) for inds,syll in enumerate(word)]
  #Origin [(syll,(len(word)-1==inds),syll[-1] in vowels,safe_list_get(wordlist,indw+1,'_')[0][0] in vowels) for indw,word in enumerate(wordlist) for inds,syll in enumerate(word)]

def buscarVocalVocalhSyll(syllablelist):
  #This line returns a list of positions in which a synaloepha can be made (including syllables starting with 'h')
  #The input is a sequence of SYLLABLES, NOT WORDS
  #This line returns a list
  return [syll[-1] in vowelsh and safe_list_get(syllablelist,inds+1,'_')[0] in vowelsh for inds,syll in enumerate(syllablelist)]

def increment(lis,gehi,moreposs,cursyll):
  if not moreposs:
    for indlis,el in enumerate(lis):
      lis[indlis]=lis[indlis]+gehi
  else: #a=[i+"-+" for i in a]+[i+"+-" for i in a]
    if gehi=='+':
      lis=[i+"-+" for i in lis]+[i+"+-" for i in lis]
    else:
      lis=[i+"--" for i in lis]
  return lis

def safe_list_get (l, idx, default):
  try:
    return l[idx]
  except IndexError:
    return default


def getRhythm(sylls,synaloephas,met):
  print "------------------------------"
  print sylls
  print synaloephas
  print met
  print "------------------------------"
  results=[""]
  synaloephas.append(False)
  real=""
  syllind=0
  metind=0
  synalind=0
  for metsyl in met:
    print "\t"+real,syllind
    adsyll=False
    if synaloephas[synalind]: #Add syllable somewhere
      adsyll=True
    results=increment(results, met[metind], adsyll, (safe_list_get(sylls,syllind,""),safe_list_get(sylls,syllind+1,"")))

    if synaloephas[synalind]: #Add syllable somewhere
      syllind=syllind+1
      synalind=synalind+1


    syllind=syllind+1
    metind=metind+1
    synalind=synalind+1
  return results

vowels=list("AEIOUÁÉÍÓÚaeiouáéíóúüyY".decode("utf8"))
vowelsh=list("AEIOUÁÉÍÓÚaeiouáéíóúüyYhH".decode("utf8"))

def main():
  WRITEXML=False
  files=glob.glob(HOMEDIR+"/CorpusSonetosSigloDeOro/*/*.xml")
  sumaccomplished=0
  sumnoverses=0
  sumnodiffmetersyllablelist=0
  sumnoextend=0
  sumnoencoger=0
  sumnoencogerconh=0
  sumnoencogerconhambiguo=0
  sumnoencogerconhnoambiguo=0
  sumnoencogerconhraro=0
  sumnoencogersinhambiguo=0
  sumnoencogersinhnoambiguo=0
  sumnobien=0
  for file in  files:
    #print file.replace("CorpusSonetosSigloDeOro","CorpusSonetosSigloDeOro-output")
    newfilename=file.replace("CorpusSonetosSigloDeOro","CorpusSonetosSigloDeOro-output")
    (parsedfile,accomplished,manually,noverses,nodiffmetersyllablelist,noextend,noencoger,noencogerconh,noencogerconhambiguo,noencogerconhnoambiguo,noencogerconhraro,noencogersinhambiguo,noencogersinhnoambiguo,nobien)=changeFile(file)
    if manually and WRITEXML:
      try:
        parsedfile.write(newfilename)
      except IOError:
        os.makedirs(os.path.dirname(newfilename))
        parsedfile.write(newfilename)
    sumaccomplished=sumaccomplished+accomplished
    sumnoverses=sumnoverses+noverses
    sumnodiffmetersyllablelist=sumnodiffmetersyllablelist+nodiffmetersyllablelist
    sumnoextend=sumnoextend+noextend
    sumnoencoger=sumnoencoger+noencoger
    sumnoencogerconh=sumnoencogerconh+noencogerconh
    sumnoencogerconhambiguo=sumnoencogerconhambiguo+noencogerconhambiguo
    sumnoencogerconhnoambiguo=sumnoencogerconhnoambiguo+noencogerconhnoambiguo
    sumnoencogerconhraro=sumnoencogerconhraro+noencogerconhraro
    sumnoencogersinhambiguo=sumnoencogersinhambiguo+noencogersinhambiguo
    sumnoencogersinhnoambiguo=sumnoencogersinhnoambiguo+noencogersinhnoambiguo
    sumnobien=sumnobien+nobien
  print "Accomplished: "+str(sumaccomplished)
  print "No of verses: "+str(sumnoverses)
  print "No of diff meter/syllablelist: "+str(sumnodiffmetersyllablelist)
  print "No of verses that need to extend: "+str(sumnoextend)
  print "No of verses that need to be reduced: "+str(sumnoencoger)
  print "No of verses that should be reduced with h: "+str(sumnoencogerconh)
  print "No of verses that should be reduced with h (ambiguous): "+str(sumnoencogerconhambiguo)
  print "No of verses that should be reduced with h (NOT ambiguous): "+str(sumnoencogerconhnoambiguo)
  print "No of verses that should be reduced with h and ARE weird: "+str(sumnoencogerconhraro)
  print "No of verses that should be reduced WITHOUT h (ambiguous):"+str(sumnoencogersinhambiguo)
  print "No of verses that should be reduced WITHOUT h (NOT ambiguous): "+str(sumnoencogersinhnoambiguo)
  print "No of equal meter/syllablelist: "+str(sumnobien)


def changeFile (filename):
    global fverses
    global frhythm
    global fmeters
    accomplished=0
    noverses=0
    nodiffmetersyllablelist=0
    noextend=0
    noencoger=0
    noencogerconh=0
    noencogerconhambiguo=0
    noencogerconhnoambiguo=0
    noencogerconhraro=0
    noencogersinhambiguo=0
    noencogersinhnoambiguo=0
    nobien=0
    parsedfile=ET.parse(filename)
    root=parsedfile.getroot()
    if "{" in root.tag:
        namespace= re.sub("\}.*", "", root.tag).replace("{","")
    else:
        namespace=''
    ns = {'ns':namespace}
    metricalDeclaration= root.findall("ns:teiHeader/ns:encodingDesc/ns:metDecl/ns:p", ns)[0].text
    if 'manually' in metricalDeclaration:
        manually=True

#        lines=root.findall("ns:text/ns:body/ns:lg/ns:l", ns)
#        for line in lines:

        linegroups=root.findall("ns:text/ns:body/ns:lg", ns)
        for linegroup in linegroups:
          lines=linegroup.findall("ns:l",ns)
          for line in lines:

            verse=re.sub("["+string.punctuation+"\¡\¿\»\«]", "", line.text).rstrip()
            verse=re.sub( '\s+', ' ', verse).strip()
            meter=line.attrib['met'].rstrip()
            rhythm=[""]
            changeablePositions=[]
            if verse != '' and meter != '':
              noverses=noverses+1
              syllabifiedVerse=analyzeWord(verse).next().decode("utf8")
              syllabifiedwords=[word.split(".") for word in syllabifiedVerse.split(" ")]
              lexicalstresses=[setstress(word) for word in syllabifiedwords]
              lsseq=''.join([''.join(wo) for wo in lexicalstresses])
              syllablelist=syllabifiedVerse.replace("."," ").split(" ")
              transform=False
              extend=False
              disambig=False
              casoh=False
              print "verso!!"
              if len(meter)!=len(syllablelist):
                nodiffmetersyllablelist=nodiffmetersyllablelist+1
                if len(syllablelist)<len(meter):
                  noextend=noextend+1
                  print "Hay que extender!!"
                  extend=True
                elif len(syllablelist)>len(meter):
                  noencoger=noencoger+1
#                  print "HAY QUE ENCOGER"
                  changeablePositions=buscarVocalVocal(syllabifiedwords)
#                  print "Hay que reducir",len(syllablelist)-len(meter),"sílabas. Podemos reducir",sum(changeablePositions),"sílabas."
#                  print "Posiciones:",changeablePositions
                  if len(syllablelist)-len(meter) > sum(changeablePositions):
                    noencogerconh=noencogerconh+1
#                    print "Busca haches!!"
                    changeablePositions=buscarVocalVocalh(syllabifiedwords)
#                    print "Hay que reducir",len(syllablelist)-len(meter),"sílabas. Podemos reducir",sum(changeablePositions),"sílabas."
#                    print "Posiciones:",changeablePositions
                    if len(syllablelist)-len(meter)<sum(changeablePositions):
                      noencogerconhambiguo=noencogerconhambiguo+1
                      print "Hay ambiguedad con h!!"
                      transform=True
                      
                      
                      print verse.encode("utf8")
#                      print changeablePositions
#                      print meter
#                      raw_input()
                      
                    elif len(syllablelist)-len(meter)==sum(changeablePositions):
                      noencogerconhnoambiguo=noencogerconhnoambiguo+1
                      print "Recorta con h!!"
                      transform=True
                    else:
                      noencogerconhraro=noencogerconhraro+1
                      print "Caso oh!!" #I just maintained this name, but it should not be called like this. Usually, we reach this case when a synaloepha should be made inside the word, such as with the word "real", which in very special cases should be pronounced as with one syllable.
                      #In order to minimally change the implementation, we will calculate the possible synaloephas from the sequence of syllables, without word division
                      print verse.encode("utf8")
                      print meter
                      print
                      changeablePositions=buscarVocalVocalhSyll(syllablelist)
                      print syllablelist,changeablePositions
                      possiblerhythms=getAmbiguousFirst(syllablelist, meter, changeablePositions)
                      mindistance=999
                      for rh in possiblerhythms:
                        try:
                          distance=sum([rh[ind]!=el for ind,el in enumerate(lsseq) if el!='?'])
                        except IndexError:
                          distance=9999
                        print "Possibility",rh,distance,mindistance
                        if distance<mindistance:
                          rhythm=[]
                          rhythm.append(rh)
                          mindistance=distance
                        elif distance==mindistance:
                          rhythm.append(rh)
                      print "Final decision:",rhythm
                      if rhythm!=['']:
                        accomplished=accomplished+1

#                      raw_input()
                      #casoh=True
                  elif len(syllablelist)-len(meter) < sum(changeablePositions):
                    noencogersinhambiguo=noencogersinhambiguo+1
                    print "Hay ambiguedad sin h!!"
                    disambig=True
                    #transform=True
                  else:
                    noencogersinhnoambiguo=noencogersinhnoambiguo+1
                    print "Recorta sin h!!"
                    transform=True
              elif len(syllablelist)==len(meter):
                nobien=nobien+1
                print "Bien!!"
                rhythm[0]=meter
                accomplished=accomplished+1
              else:
                print "Error!"
                exit()
            else:
              transform=False
            print verse.encode("utf8"), syllabifiedwords
            #print changeablePositions
            print meter
            if transform:
              print "Transform!"
#              possiblerhythms=getRhythm(syllablelist, changeablePositions, meter)
              possiblerhythms=getAmbiguousFirst(syllablelist, meter, changeablePositions)
              accomplished=accomplished+1
#              print lsseq
#              print rhythm
#              print [len(i) for i in rhythm], len(lsseq)
#              if sum([len(i)!=len(lsseq) for i in rhythm])>0:
#                print "There is a weird case."
              mindistance=999
              for rh in possiblerhythms:
                try:
                  distance=sum([rh[ind]!=el for ind,el in enumerate(lsseq) if el!='?'])
                except IndexError:
                  distance=9999
                print "Possibility",rh,distance,mindistance
                if distance<mindistance:
                  rhythm=[]
                  rhythm.append(rh)
                  mindistance=distance
                elif distance==mindistance:
                  rhythm.append(rh)
#                  raw_input()
#                print rh
#                print lsseq
#                print distance
              #raw_input()
            if disambig:
              print "Disambig!!"
              accomplished=accomplished+1
              possiblerhythms=getAmbiguousFirst(syllablelist, meter, changeablePositions)
              #print "Possibilities: ",possiblerhythms
              mindistance=999
              for rh in possiblerhythms:
                try:
                  distance=sum([rh[ind]!=el for ind,el in enumerate(lsseq) if el!='?'])
                except IndexError:
                  distance=9999
                print "Possibility",rh,distance,mindistance
                if distance<mindistance:
                  rhythm=[]
                  rhythm.append(rh)
                  mindistance=distance
                elif distance==mindistance:
                  rhythm.append(rh)
            print "|".join(rhythm),rhythm
            print 
            line.attrib['real']="|".join(rhythm)
            syllabifiedVerseStr=' '.join(['.'.join(word) for word in syllabifiedwords])
            fverses.write(syllabifiedVerseStr.encode("utf8")+"\n")
            frhythm.write("|".join(rhythm).encode("utf8")+"\n")
            fmeters.write(meter.encode("utf8")+"\n")
          fverses.write("\n")
          frhythm.write("\n")
          fmeters.write("\n")
        fverses.write("\n")
        frhythm.write("\n")
        fmeters.write("\n")
    else:
        manually=False
    return (parsedfile,accomplished,manually,noverses,nodiffmetersyllablelist,noextend,noencoger,noencogerconh,noencogerconhambiguo,noencogerconhnoambiguo,noencogerconhraro,noencogersinhambiguo,noencogersinhnoambiguo,nobien)
#    noverses=0
#    nodiffmetersyllablelist=0
#    noextend=0
#    noencoger=0
#    noencogerconh=0
#    noencogerconhambiguo=0
#    noencogerconhnoambiguo=0
#    noencogerconhraro=0
#    noencogersinhambiguo=0
#    noencogersinhnoambiguo=0


if __name__=='__main__':
  if len(sys.argv)>1:
    fverses=open("kkverses.txt","w")
    frhythm=open("kkanalyses.txt","w")
    fmeters=open("kkmeters.txt","w")
    print changeFile (sys.argv[1])
    fverses.close()
    frhythm.close()
    fmeters.close()
  else:
    fverses=open("verses.txt","w")
    frhythm=open("analyses.txt","w")
    fmeters=open("meters.txt","w")
    main()
    fverses.close()
    frhythm.close()
    fmeters.close()
  #main_old()

  

