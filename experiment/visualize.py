
# -*- coding: utf-8
import xml.etree.ElementTree as ET
import sys
import re
import string
import foma
import os

FST=foma.FST()
FSM=FST.load("/home/magirrezaba008/Dropbox/thesis/syllableGoldStandard/ruleSyllabifiers/silabaEs.fst")

from extract import analyzeWord

#MODIFIED FROM http://stackoverflow.com/questions/287871/return-in-terminal-with-colors-using-python
def prRed(prt): return("\033[91m{}\033[00m" .format(prt.encode("utf8")))
def prGreen(prt): return("\033[92m{}\033[00m" .format(prt.encode("utf8")))
def prYellow(prt): return("\033[93m{}\033[00m" .format(prt.encode("utf8")))
def prLightPurple(prt): return("\033[94m{}\033[00m" .format(prt.encode("utf8")))
def prPurple(prt): return("\033[95m{}\033[00m" .format(prt.encode("utf8")))
def prCyan(prt): return("\033[96m{}\033[00m" .format(prt.encode("utf8")))
def prLightGray(prt): return("\033[97m{}\033[00m" .format(prt.encode("utf8")))
def prBlack(prt): return("\033[98m{}\033[00m" .format(prt.encode("utf8")))
rows, columns = os.popen('stty size', 'r').read().split()

def centeredmeter(met,sp):
  lensp=len(sp)
  lenmet=len(met)
  startpos=lensp-(lensp/2)-(lenmet/2)
  return sp[:startpos]+met+sp[(startpos+lenmet):]

def threecolumns(text1,text2,meter):
  text2len=len(text2)
  text1len=len(text1)
  
  nofspaces=((int(columns)-text1len-text1len))
  space=""
  for el in xrange(nofspaces-20):
    space=space+" "

  print text1.encode("utf8")+centeredmeter(meter,space)+text2

def main(file):

  parsedfile=ET.parse(file)
  root=parsedfile.getroot()
  if "{" in root.tag:
    namespace= re.sub("\}.*", "", root.tag).replace("{","")
  else:
    namespace=''
  ns = {'ns':namespace}
  lines=root.findall("ns:text/ns:body/ns:lg/ns:l", ns)
  linegroups=root.findall("ns:text/ns:body/ns:lg", ns)

  for linegroup in linegroups:
    
    for line in linegroup:
      verse=re.sub("["+string.punctuation+"\¡\¿\»\«]", "", line.text).rstrip()
      verse=re.sub( '\s+', ' ', verse).strip()
      syllabifiedVerse=analyzeWord(verse).next().decode("utf8")
#      print line.text,line.attrib['met'],"\t\t\t",line.attrib['real']
      met=line.attrib['met'].rstrip()
      real=line.attrib['real'].rstrip()
      sylls=syllabifiedVerse.replace("."," ").split(" ")
#      print type(sylls)
#      print [type(i) for i in sylls]
      colouredText=""
#      print sylls
#      print real
      if real!='':
        for indsyll,syll in enumerate(sylls):
        
          if real[indsyll]=='+':
#            print prGreen(syll),
            colouredText=colouredText+prGreen(syll)+" "
          else:
#            print prRed(syll),
            colouredText=colouredText+prRed(syll)+" "
#        print
        threecolumns(line.text, colouredText, met)
      else:
        print line.text.encode("utf8"),met,sylls
    print 
    print 
if __name__=='__main__':
  main(sys.argv[1])
  
