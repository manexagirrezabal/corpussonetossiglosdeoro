
#sent= "que so la e lla lo co no ce y sien te".decode("utf8").split(" ")
#meter="-+-+---+-+-"
#synal=[False,False,True,False,False,False,False,False,True,False,False,False]

#sent= "ka ka ik pi pi".decode("utf8").split(" ")
#meter="--+++"
#synal=[False,True,False,False,False]

def getAmbiguous (sylls,met,changeable,syllidx,metidx,result):
  if metidx>=len(met):
    return []
  if len(sylls)-1<=syllidx: #I'm doubting if it should be like this. It was == before...
    return [res+met[metidx] for res in result]
  else:
    if changeable[syllidx]:
      #Overgenerate, and then, at the end we filter them using as evaluation function the distance between the rhythm and the lexical stress sequence (leaving monosyllables untagged).
      res1=getAmbiguous(sylls,met,changeable,syllidx+2,metidx+1,[res+"-"+met[metidx] for res in result])
      res2=getAmbiguous(sylls,met,changeable,syllidx+2,metidx+1,[res+met[metidx]+"-" for res in result])
      res3=getAmbiguous(sylls,met,changeable,syllidx+1,metidx+1,[res+met[metidx] for res in result])
      return res1+res2+res3
    else:
      interres=[res+met[metidx] for res in result]
      return getAmbiguous(sylls,met,changeable,syllidx+1,metidx+1,interres)

def getAmbiguousFirst(sent,meter,synal):
  return list(set(getAmbiguous(sent,meter,synal,0,0,[''])))

if __name__=='__main__':
  sent= "que so la e lla lo co no ce y sien te".decode("utf8").split(" ")
  meter="-+-+---+-+-"
  synal=[False,False,True,False,False,False,False,False,True,False,False,False]
  sent=[u'por', u'que', u'es', u'pe', u'ra', u'tal', u'vez', u'a', u'll\xed', u'en', u'cen', u'di', u'da']
  meter="--+-++-+-+-"
  synal=[False, True, False, False, False, False, False, False, True, False, False, False, False]
  sent=[u'i', u'mi', u'ta', u'r\xe1', u'si', u'gui\xe9n', u'do', u'os', u'mi', u'al', u'be', u'dr\xed', u'o']
  meter="---+-+---+-"
  synal=[False, False, False, False, False, False, True, False, True, False, False, True, False]
  sent= "a bre via y el O rien te se le hu mi lla".decode("utf8").split(" ")
  meter="-+---+---+-"
  synal=[False, False, True, True, False, False, False, False, False, False, False, False, False]
  print getAmbiguousFirst(sent,meter,synal)


