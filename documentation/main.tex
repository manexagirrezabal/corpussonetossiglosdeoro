\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{url}

\begin{document}



\centerline{\sc \large A heuristic to change from metrical information to rhythm in Spanish}
\vspace{.5pc}
\centerline{\sc MANEX AGIRREZABAL}
\vspace{2pc}

\abstract{In this document we show a heuristic for the transformation of a corpus of Spanish poetry in order to be analyzed using some tools that we developed for English.}

\section{Motivation}
The interest on this heuristic comes from the impossibility of applying some algorithms developed for the rhythmic analysis of English poetry
because of the use of synaloepha in Spanish poetry.
This does not mean that this kind of devices are not used in English, as syllable addition is quite typical too.
The issue is that in the corpus that we are using for our experiments in English, all syllables are marked with a level of stress, and because of that we decided to mark all the syllables (marking the syllables around synaloephas as unstressed).

\section{Corpus}
The corpus we are using for this work \cite{navarrocorpus} has been created and is mantained by Borja Navarro Colorado and it is available on GitHub.\footnote{\url{https://github.com/bncolorado/CorpusSonetosSigloDeOro}}
This corpus is a collection of sonnets from the Spanish Golden Age and it is composed of 82593 verses, and they include metrical information about each line, calculated automatically using a scansion system. From all the verses, 1921 have been manually checked at this moment (09/06/2016), which will be the ones that we are interested. We are aplying the heuristic to the manually checked part.


\section{The task}
For each line we have to return a stress sequence that will be the allegedly produced sequence when the poem is read aloud.
The inputs that we have in this problem are a line of poem, such as,

\begin{center}
\textit{que o no podréis de lástima escucharme,}
\end{center}

\noindent along with a sequence of stress values.

\begin{center}
- + - + - + - - - + -
\end{center}

By dividing the verse in syllables,

\begin{center}
\textit{que o no po.dréis de lás.ti.ma es.cu.char.me}
\end{center}

\noindent we can easily realize that the assignment of the stresses directly to each syllable will leave syllables without assigning any stress value.
This is evident because there are 13 syllables in the verse and in the metrical values there are 11 elements (It is a hendecasyllable).
This is the input to our heuristic, a verse and a sequence of stresses.

By applying synaloepha to the poem above, the stresses would be assigned in this way:

\begin{center}
\textit{que\_o nO po.drÉis de lÁs.ti.ma\_es.cu.chAr.me}
\end{center}

The main goal is to add unstressed syllables in a way that the stressed syllables will be kept. Hence, for the above example a possible solution is

\begin{center}
- - + - + - + - - - - + -
\end{center}

\noindent where each syllable is marked with a level of stress, although, obviously, the time spent to produce the syllables involved in synaloephas is much shorter.

\section{Implementation details}
As the corpus of Spanish poetry is not syllabified we had to apply a syllabification algorithm for Spanish written in foma\cite{huldenfoma}, a finite state toolkit and library, and is mostly based on the syllabification procedure presented in \cite{agirrezabal2012finite}, which relies on the maximum onset principle together with the sonority hierarchy.
Once the corpus is syllabified, we count the number of syllables and compare it with the length of the metrical tagging.

\subsection*{NOSYLLABLES == NOSTRESSES (589 cases)}
If the number of syllables in the line and the number of stresses in the metrical tagging is equal, this means that there is no need of synaloepha, so the metrical values will be returned directly. We have observed 589 cases in our corpus.


\subsection*{NOSYLLABLES != NOSTRESSES (1332 cases)}
If the number of syllables and the number of stresses is not the same (1332 verses), some changes must be made:
\begin{enumerate}
\item If the number of syllables is higher than the number of stresses: Some syllables must be joined, by using synaloepha (1312 verses).
\item If the number of stresses is higher than the number of syllables: It is a special case that we did not expect (20 verses) and will have to be resolved by hand.
\end{enumerate}

In the first case, we calculate the potential synaloephas that can be made in the verse. Firstly we try to perform synaloepha without including the words that start with the letter 'h' \cite[p. 26]{guianotacion}.
If performing all the allowed synaloephas the number of syllables is equal to the metrical syllables, then a solution can be given. This happens in 1166 cases.
For example, in the case

\begin{center}
\textit{no sé ya qué ha.cer.me en mal ta.ma.ño}

+ + + + - + - + - + -
\end{center}

the synaloepha between the syllables \textit{me} and \textit{en} will be chosen and the syllables involving the synaloepha will be unstressed.
If making this changes, the number of syllables and the metrical structures length do not coincide, we may need more changes.
In such case, synaloephas involving the letter `h' are performed, which in the corpus there are 146 cases.
E.g. in the next example,

\begin{center}
\textit{en es.te in.cen.dio her.mo.so que par.ti.do}

- + - + - + - - - + -
\end{center}

\noindent performing contraction between the syllables \textit{te} and \textit{in} will not suffice because the verse still has 12 syllables.
Because of that, we apply synaloephas that include the letter 'h' at the beginnning, so the syllables \textit{dio} and \textit{her} will be joined.
The resulting analysis will be this:

\begin{center}
\textit{en Es.te in.cEn.dio her.mO.so que par.tI.do}
\end{center}

\bigskip

Until now, we have seen cases in which making all possible contractions we could assign perfectly the stresses to each syllable. Unfortunately, it is not so straighforward in all the cases. In the verse

%Example with ambiguity and lexical stress use.
\begin{center}
\textit{y es.tre.cho cuan.do es.tu.vo so.bre mí}

- + - - - + - - - + -
\end{center}

\noindent there are 12 syllables and two possible synaloephas that can be made. We could join \textit{y} and \textit{es} or \textit{do} and \textit{es}. In these cases where there are two possible synaloephas, we first generate the possible scansions and evaluate them according to the lexical stress sequence of the verse. We calculate the simple edit distance between the two strings by using only substitution. The lexical stress sequence, the possible scansions and the distances can be seen in table \ref{table:overgeneration}. For the calculation of the lexical stress, we consider accented monosyllabic words as stressed, but the ones that do not have any accent, are tagged with a question mark, indicating that it can be either stressed or unstressed.\footnote{We know that there is a weakness in the case that we have a verb like ``voy'' which is not accented and it should be stressed..}


\begin{table}[ht]
\centering
\caption{Possible stress sequences for the example in (XXX)}
\label{table:overgeneration}
\begin{tabular}{l|cccccccccccc|r}
\hline
Lexical stresses  & ? & - & + & - & + & - & - & + & - & + & - & + &   \\
\hline
Possibility no. 1 & - & + & - & - & - & - & + & - & - & - & + & -  & 8 \\
Possibility no. 2 & - & + & - & - & - & + & - & - & - & - & + & -  & 8 \\
Possibility no. 3 & - & - & + & - & - & - & + & - &  - & - & + & - & 6 \\
Possibility no. 4 & - & - & + & - & - & - & - & + & - & - & - & + & 2 \\
\hline
\end{tabular}
\end{table}

Once that we have generated the possible scansions and evaluated them, we choose the one with the minimum edit distance (allowing only substitution) to the lexical stress sequence. If there is not only one minimum but more, we set the verse as ambiguous and accept several scansions, such as in the case,

\begin{center}
\textit{si no es en ha.ber si.do yo guar.da.do}

- + - - + + - +  - + -
\end{center}

\noindent where two possible analyses can be got:

\begin{center}
- + - - - + + - + -  + -

or

- - + - - + + -  + - + -
\end{center}

\section{Special cases + error analysis}
Although the algorithm seems to be correct in multiple cases, there are still some verses in which it can not give a correct solution.
Some weaknesses come from the ambiguity of the syllable division process. For instance, in this poem by Góngora,

\begin{center}
\textit{Este cíclope, no siciliano,}

\textit{del microcosmo sí, orbe postrero;}

\textit{esta antípoda faz, cuyo hemisferio}

\textit{zona divide en término italiano;}
\end{center}

\noindent even the rhyming part does not coincide in the rhythmic pattern (first and last lines), which makes (practically) impossible for the machine to calculate correctly the stress structure of the first verse. In the case of the words including the vowel sequence 'ue' or 'ia' it can be considered a dipthong or not, for example, ``cru.e.les'' and ``pue.do''. There are some singular cases in which triphthongs must be made, and we have not covered such cases, e.g., ``Eres robusto escándalo a orgullosa'', where the pronunciation should be ``es.cán.da.lo\_a\_or.gu.llo.sa''. We analyzed the results of the heuristic so that to check its correctness and fix errors. The main source of errors was that sometimes a triple vowel synaloepha must be made, for which our heuristic is not prepared. Fortunately, this is not very common in this corpus, so it can be resolved manually. Last but not least, we should mention that we have not found a solution for the 20 verses that will need to be resolved by hand.

At the end, we also realized that our rule-based syllabifier did not perform perfectly, as we expected. It does not divide a word if the left syllable ends with "ns" and the second starts with anything (we have seen the cases like "instante" $\rightarrow$ "instan.te" and "inscripción" $\rightarrow$ "inscrip.ción"). We should fix the syllabifier as soon as possible to avoid future errors.

confianza $\rightarrow$ con.fianza

halagüeña $\rightarrow$ ha.lagüe.ña (ü is the problem)

\section{Discussion}
In this document a heuristic for the modification of a metrically annotated corpus has been presented. The heuristic has some weaknesses, but we think that it can help us in the transformation of the Spanish corpus so that we can apply Machine Learning tools as we did in the case of the corpus in English.
Now the main important task is to convert the corpus and manually check the correctness of the generated sequences so that we know the validity of the heuristic.

\begin{thebibliography}{9}
\bibitem{agirrezabal2012finite} 
Manex Agirrezabal, Inaki Alegria, Bertol Arrieta, Mans Hulden
\textit{Finite-state technology in a verse-making tool}.
10th International Workshop on Finite State Methods and Natural Language Processing, p. 35.
\bibitem{guianotacion}
Borja Navarro Colorado, María Ribes Lafoz, Noelia Sánchez
\textit{Guía de anotación métrica}.
\bibitem{huldenfoma}
Mans Hulden
\textit{Foma: a finite-state compiler and library}.
Proceedings of the 12th Conference of the European Chapter of the Association for Computational Linguistics: Demonstrations Session. Association for Computational Linguistics, 2009.
\bibitem{navarrocorpus}
Borja Navarro Colorado
\textit{A computational linguistic approach to Spanish Golden Age Sonnets: metrical and semantic aspects}
Proceedings of NAACL-HLT Fourth Workshop on Computational Linguistics for Literature, Denver, Colorado, pages 105–113
\end{thebibliography}

\end{document}
