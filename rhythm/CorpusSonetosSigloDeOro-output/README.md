This is the modified part of the corpus. In this part, each syllable has its own stress level, so that the models presented in [Agirrezabal et al. (2016)](https://www.aclweb.org/anthology/C/C16/C16-1074.pdf) can be applied to Spanish poetry.


##Team
This project has been carried out at the [University of Alicante](http://www.ua.es) by Borja Navarro Colorado, María Ribes Lafoz and Noelia Sánchez, with the collaboration of Sara Trigueros.

The corpus has been modified by Manex Agirrezabal.

##How to cite this corpus
If you would like to cite this corpus for academic research purposes, please use these references:

Navarro-Colorado, Borja; Ribes Lafoz, María, and Sánchez, Noelia (2015) "Metrical annotation of a large corpus of Spanish sonnets: representation, scansion and evaluation" 10th edition of the Language Resources and Evaluation Conference 2016 Portorož, Slovenia. (Submitted!). ([PDF](http://www.dlsi.ua.es/~borja/navarro2016_MetricalPatternsBank.pdf))

##Further Information
If you require further information about the project, please consult the Annotation Guide -in Spanish- ([PDF](http://www.dlsi.ua.es/~borja/GuiaAnotacionMetrica.pdf)) or the following papers:

- Navarro-Colorado, Borja (2015) "A computational linguistic approach to Spanish Golden Age Sonnets: metrical and semantic aspects" [Computational Linguistics for Literature NAACL 2015](https://sites.google.com/site/clfl2015/), Denver (Co), USA ([PDF](https://aclweb.org/anthology/W/W15/W15-0712.pdf)).

- Navarro-Colorado, Borja; Ribes Lafoz, María; Trigueros, Sara J.; y Sánchez, Noelia (2015) "Compilación y anotación métrica de un corpus de sonetos del Siglo de Oro" [Humanidades Digitales Hispánicas](http://hdh2015.linhd.es/) UNED, Madrid.

##License
The metrical annotation of this corpus is licensed under a Creative Commons Attribution-Non Commercial 4.0 International License.

About the sonnets, "this digital object is protected by copyright and/or related rights. This digital object is accessible without charge, but its use is subject to the licensing conditions set by the organization giving access to it. Further information available at http://www.cervantesvirtual.com/marco-legal/ ".
